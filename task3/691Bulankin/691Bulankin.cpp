#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <cassert>

#include <iostream>
#include <vector>
#include <random>
#include <sys/time.h>
#include <ctime>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/LightInfo.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"

#include "Rules.h"
#include "LSystem.h"
#include "TreeMesh.h"

namespace {

    float frand(float a, float b) {

        static std::default_random_engine generator;
        std::uniform_real_distribution<float> distribution(a, b);

        return distribution(generator);
    }

}

class SampleApplication : public Application {
public:
    const int NUM_TREES = 50;

    MeshPtr _tree;
    MeshPtr _leaves;
    MeshPtr _sphere;
    MeshPtr _ground;

    ShaderProgramPtr _shader;

    // Directional light angle
    float _phi = glm::pi<float>() * 0.0f;
    float _theta = glm::pi<float>() * 0.25f;

    // Directional light parameters
    LightInfo _light;

    TexturePtr _barkTexture;
    TexturePtr _leafTexture;
    TexturePtr _grassTexture;

    GLuint _sampler;
    GLuint _leafSampler;
    GLuint _grassSampler;

    std::vector<glm::vec3> _positionsVec3;

    void makeScene() override {
        Application::makeScene();

        auto Rules = RulesApplier{};
        Rules.AddRule('F', "F[-F]&[F]^^[-F]&[+F][F]");

        auto lsystem = Rules.Transform(4, std::string{"F"});
        auto sticks = ParseLSystem(lsystem);

        std::tie(_tree, _leaves) = MakeTreeAndLeavesMeshes(sticks);

        auto modelMatrix = glm::scale(
            glm::mat4(1.0f),
            glm::vec3(0.5f, 0.5f, 0.5f)
        );

        _tree->setModelMatrix(modelMatrix);
        _leaves->setModelMatrix(modelMatrix);

        //=========================================================

        const float size = 15.0f;
        for (unsigned int i = 0; i < NUM_TREES; i++)
        {
            _positionsVec3.push_back(glm::vec3(frand( -size, size), frand( -size, size),0));
            std::cerr << _positionsVec3.back().x << " " << _positionsVec3.back().z << std::endl;
        }

        _ground = makeGroundPlane(30.0f, 5.0f);
        _ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.0f)));

        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>(
            "691BulankinData3/shader.vert",
            "691BulankinData3/shader.frag"
        );

        //=========================================================
        //Инициализация значений переменных освщения
        // Direction pointing outwards, toward the light source
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_theta), glm::sin(_phi) * glm::cos(_theta));
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(0.3, 0.3, 0.3);

        //=========================================================
        //Загрузка и создание текстур
        _barkTexture = loadTexture("691BulankinData3/bark2.jpg");
        _leafTexture = loadTexture("691BulankinData3/leaves3.png");
        _grassTexture = loadTexture("691BulankinData3/grass1.jpg");


        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        for (auto sampler : {_sampler, _leafSampler, _grassSampler}) {
            glGenSamplers(1, &sampler);
            glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        }

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    void draw() override {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        glm::vec3 lightDirCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));

        _shader->setVec3Uniform("light.dir", lightDirCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        {
            _shader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
            _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _grassSampler);
            _grassTexture->bind();
            _ground->draw();
        }

        auto drawMesh = [&](const MeshPtr& mesh, const TexturePtr& tx, GLuint sampler, bool isLeaf) {
            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, sampler);
            tx->bind();
            _shader->setIntUniform("diffuseTex", 0);
            _shader->setVec3UniformArray("positions", _positionsVec3);

            //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
            _shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
            _shader->setMat3Uniform(
                "normalToCameraMatrix",
                glm::transpose(
                    glm::inverse(
                        glm::mat3(
                            _camera.viewMatrix * mesh->modelMatrix()
                        )
                    )
                )
            );
            _shader->setIntUniform("isLeaf", isLeaf);
            mesh->drawInstanced(_positionsVec3.size());
        };

        drawMesh(_tree, _barkTexture, _sampler, false);
        drawMesh(_leaves, _leafTexture, _leafSampler, true);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
};

int main()
{
    srand (time(NULL));
    SampleApplication app;
    app.start();

    return 0;
}

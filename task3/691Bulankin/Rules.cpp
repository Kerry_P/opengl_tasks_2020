#include "Rules.h"


std::string RulesApplier::Transform(int times, const std::string& source) const {
    const std::string* sourcePtr = &source;

    std::string result, temp;

    for (int i = 0; i < times; ++i) {
        Transform(*sourcePtr, std::back_inserter(temp));
        result.swap(temp);
        sourcePtr = &result;
        temp.clear();
    }

    return result;
}

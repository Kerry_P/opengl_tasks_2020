#pragma once

#include <vector>
#include <string>

#include "TreeMesh.h"

std::vector<TreeStick3> ParseLSystem(const std::string& src);

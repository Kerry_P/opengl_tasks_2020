#include "LSystem.h"

#include <cmath>
#include <stack>
#include <vector>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/norm.hpp>
#include <windef.h>


struct State {
    glm::vec3 pos;
    glm::vec3 dir;
    glm::vec3 angles;

    float angle;
    float invert;
};

void UpdateState(State& state, const glm::vec3& dir, int level) {
//    state.pos += dir * (0.8f + rand() / (RAND_MAX + 1.f) * 0.4f);
//    state.dir *= 0.8 + rand() / (RAND_MAX + 1.) * 0.2;
//    state.angle *= 1. + rand() / (RAND_MAX + 1.) * 0.2;
    state.pos += dir;
    state.dir *= 0.9;
    state.angle *= 1.1;
}

glm::vec3 Step(const State& state) {
    auto ret = glm::rotateX(state.dir, state.angles.x);
    ret = glm::rotateY(ret, state.angles.y);
    ret = glm::rotateZ(ret, state.angles.z);
    return ret;
}

std::vector<TreeStick3> ParseLSystem(const std::string& src) {
    State state;
    std::stack<State> st;
    glm::vec3 d;
    std::vector<TreeStick3> sticks;

    state.pos = glm::vec3(0, 0, 0);
    state.dir = glm::vec3(0, 0, 1);
    state.angle = 30 * glm::pi<float>() / 180;
    state.invert = 1;
    state.angles = glm::vec3(0, 0, state.angle);

    for (auto c : src) {
        switch (c) {
        case '[':       // push state
            st.push(state);
            break;

        case ']':       // pop state
            state = st.top();
            st.pop();
            break;

        case 'F':
            d = Step(state);

            sticks.emplace_back();
            sticks.back().Level = st.size();
            sticks.back().A = state.pos;
            sticks.back().B = state.pos + d;

            UpdateState(state, d, st.size());
            break;

        case 'X':
            break;

        case 'f':
            UpdateState(state, Step(state), st.size());
            break;

        case '!':
            state.invert *= -1;
            break;

        case '+':
            state.angles.y += state.invert * state.angle;
            break;

        case '-':
            state.angles.y -= state.invert * state.angle;
            break;

        case '&':
            state.angles.x += state.invert * state.angle;
            break;

        case '^':
            state.angles.x -= state.invert * state.angle;
            break;

        case '<':
            state.angles.z += state.invert * state.angle;
            break;

        case '>':
            state.angles.z -= state.invert * state.angle;
            break;

        default:
            throw std::logic_error("unexpected char " + std::string(1, c));
        }
    }

    return sticks;
}

set(SRC_FILES
    common/Application.cpp
    common/Application.hpp

    common/Camera.cpp
    common/Camera.hpp

    common/Mesh.cpp
    common/Mesh.hpp

    common/ShaderProgram.cpp
    common/ShaderProgram.hpp

    common/Texture.cpp
    common/Texture.hpp

        Rules.h
        Rules.cpp

    LSystem.h
    LSystem.cpp

    TreeMesh.h
    TreeMesh.cpp

    691Bulankin.cpp
)

#include_directories(691BulankinData1)
include_directories(common)
include_directories(../external)

#MAKE_SAMPLE(691Bulankin 1 "${SRC_FILES}")
#MAKE_SAMPLE(gl1_SimpleTriangle)

MAKE_OPENGL_TASK(691Bulankin 2 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(691Bulankin2 stdc++fs)
endif()
#pragma once

#include <vector>

#include "common/Mesh.hpp"

#include <glm/glm.hpp>


template<class Point>
struct SegmentT {
    Point A;
    Point B;
};

using Segment3 = SegmentT<glm::vec3>;

template<class Point>
struct TreeStickT : public SegmentT<Point> {
    int Level;
};

using TreeStick3 = TreeStickT<glm::vec3>;


std::pair<MeshPtr, MeshPtr> MakeTreeAndLeavesMeshes(const std::vector<TreeStick3>& sticks);

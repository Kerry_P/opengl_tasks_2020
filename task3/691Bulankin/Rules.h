#pragma once

#include <random>
#include <string>
#include <unordered_map>
#include <vector>
#include <iterator>

using Rules = std::unordered_map<char, std::vector<std::string>>;

template<class ForwardIt>
ForwardIt ChooseOne(ForwardIt begin, ForwardIt end) {
    auto d = std::distance(begin, end);

    static auto engine = std::default_random_engine();
    auto distr = std::uniform_int_distribution<int>(0, d - 1);

    auto shift = distr(engine);

    return std::next(begin, shift);
}

class RulesApplier {
public:
    RulesApplier& AddRule(char lhs, std::string rhs) {
        Rules[lhs].emplace_back(std::move(rhs));
        return *this;
    }

    std::string Transform(int times, const std::string& source) const;

    std::string Transform(const std::string& source) const {
        std::string result;
        Transform(source, std::back_inserter(result));
        return result;
    }

    template<class OutputIt>
    OutputIt Transform(const std::string& source, OutputIt out) const {
        for (char c : source) {
            out = Transform(c, out);
        }
        return out;
    }

    template<class OutputIt>
    OutputIt Transform(char c, OutputIt out) const {
        auto it = Rules.find(c);
        if (it != Rules.end()) {
            const auto& here = it->second;
            auto chosen = ChooseOne(here.begin(), here.end());
            return std::copy(chosen->begin(), chosen->end(), out);
        }
        *out = c;
        return ++out;
    }

private:
    Rules Rules;
};

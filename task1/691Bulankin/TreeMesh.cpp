#include "TreeMesh.h"

namespace {

void MakeCone(
    const glm::vec3& a,
    const glm::vec3& b,
    float r1,
    float r2,
    int N,
    std::vector<glm::vec3>& vertices,
    std::vector<glm::vec3>& normals) {

    assert(r1 > 0.0 && r2 > 0.0);
    assert(N >= 3);

    auto ab = b - a;

    // Perpendiculars to ab
    auto p1 = glm::vec3(-ab.y, ab.x, 0);
    if (glm::length(p1) < 1e-5) {
        p1 = glm::vec3(-ab.z, 0, ab.x);
    }
    if (glm::length(p1) < 1e-5) {
        p1 = glm::vec3(0, -ab.z, ab.y);
    }
    assert(glm::length(p1) >= 1e-5);
    p1 = normalize(p1);

    auto p2 = normalize(cross(ab, p1));

    auto polar_radius = [&](float phi, float r) {
        return p1 * glm::cos(phi) * r + p2 * glm::sin(phi) * r;
    };

    auto add3Normals = [&](glm::vec3 v) {
        v = normalize(v);
        for (int i = 0; i < 3; ++i) {
            normals.push_back(v);
        }
    };

    auto addTriangle = [&](glm::vec3 a, glm::vec3 b, glm::vec3 c) {
        vertices.push_back(a);
        vertices.push_back(b);
        vertices.push_back(c);
        add3Normals(cross(a - b, a - c));
    };

    for (unsigned int i = 0; i < N; ++i) {
        float phi1 = 2.0f * glm::pi<float>() * i / N;
        float phi2 = 2.0f * glm::pi<float>() * (i + 1) / N;

        auto bot1 = a + polar_radius(phi1, r1);
        auto bot2 = a + polar_radius(phi2, r1);

        auto top1 = b + polar_radius(phi1, r2);
        auto top2 = b + polar_radius(phi2, r2);

        // Bottom face triangles
        addTriangle(bot2, bot1, a);

        // Top face triangles
        addTriangle(top2, top1, b);

        // First side triangle
        addTriangle(bot1, bot2, top1);

        // Second side triangle
        addTriangle(top2, top1, bot2);
    }
}


MeshPtr MakeMesh(
        const std::vector<glm::vec3>& vertices,
        const std::vector<glm::vec3>& normals
    ) {

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

}


MeshPtr MakeTreeMesh(const std::vector<TreeStick3>& sticks) {
    constexpr int DETAIL = 35;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    auto get_radius = [](int level) {
        constexpr float BARK_RADIUS = 0.18f;
        constexpr float INITIAL_RADIUS = 0.13f;
        constexpr float RADIUS_SCALE = 0.3f;

        float radius = level == 0 ? BARK_RADIUS : INITIAL_RADIUS;
        for (int i = 0; i < level - 1; i++) {
            radius *= RADIUS_SCALE;
        }
        return radius;
    };

    for (auto& stick : sticks) {
        MakeCone(
            stick.A,
            stick.B,
            get_radius(stick.Level),
            get_radius(stick.Level + 1),
            DETAIL,
            vertices,
            normals
        );
    }

    return MakeMesh(vertices, normals);
}
